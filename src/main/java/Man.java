import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public final class Man extends Human {
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    Date birthdayDateFormat;

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String date, int iq) throws ParseException {
        super(name, surname, iq);
        birthdayDateFormat = format.parse(date);
    }

    public Man(String name, String surname, int iq, HashMap<DayOfWeek,String> schedule) {
        super(name, surname, iq, schedule);
    }

    public Man() {
    }

    public void setBirthdayDateFormat(Date birthdayDateFormat) {
        this.birthdayDateFormat = birthdayDateFormat;
    }

    public String birthday() {
        String birthday = format.format(birthdayDateFormat);
        return birthday;
    }

    public void repairCar() {
        System.out.println("I need to repair my car");
    }

    @Override
    public void greetPet() {
            System.out.println("Привет, " + super.getFamily().getPet().getNickname() + "!");
    }

    @Override
    public String toString() {
        return "Man {name = " + this.getName() + ", " + "surname = " + this.getSurname() + ", " + "birthday = " + this.birthday() + ", " + "iq = " + this.getIq() + ", " + super.getSchedule() + "}";
    }

}
