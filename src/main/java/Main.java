import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {

        Man viktor = new Man("Viktor","Munko","03/03/1982",89);
        viktor.setIq(150);
        HashMap<DayOfWeek,String> scheduleViktor = new HashMap<DayOfWeek,String>();
        scheduleViktor.put(DayOfWeek.SATURDAY,"Play football");
        viktor.setSchedule(scheduleViktor);
        Woman halyna = new Woman("Halyna","Munko","03/03/1988",89);
        halyna.setIq(150);
        HashMap<DayOfWeek,String> scheduleHalyna = new HashMap<DayOfWeek,String>();
        scheduleHalyna.put(DayOfWeek.SATURDAY,"Go shopping");
        halyna.setSchedule(scheduleHalyna);
        Family familyWick = new Family(halyna, viktor);
        familyWick.setMother(halyna);
        Dog dog = new Dog("Rej",3,95, new HashSet<String>(Set.of("play")));
        dog.getSpecies().setNumberOfPaw(4);
        familyWick.setPet(dog);
        Man timyr = new Man("Timyr","Munko","03/03/1686",89);
        familyWick.addChild(timyr);
        timyr.setFamily(familyWick);
        Man bohdan = new Man("Bohdan","Munko","03/03/1686",89);
        familyWick.addChild(bohdan);
        bohdan.setFamily(familyWick);


        Man vova = new Man("Volodimer","Tkachuk","03/03/1685",89);
        vova.setIq(200);
        HashMap<DayOfWeek,String> scheduleVolodimer = new HashMap<DayOfWeek,String>();
        scheduleVolodimer.put(DayOfWeek.SATURDAY,"Play computer games");
        vova.setSchedule(scheduleVolodimer);
        Woman natali = new Woman("Natali","Tkachuk","03/03/1690",89);
        natali.setIq(150);
        HashMap<DayOfWeek,String> scheduleMonica = new HashMap<DayOfWeek,String>();
        scheduleMonica.put(DayOfWeek.SUNDAY,"Go to the bar");
        natali.setSchedule(scheduleMonica);
        Family familyVolodimer = new Family(natali, vova);
        DomesticCat cat = new DomesticCat("Markiz",4,50, new HashSet<String>(Set.of("sleep","eat")));
        cat.getSpecies().setNumberOfPaw(4);
        familyVolodimer.setPet(cat);
        Man matvij = new Man("Matvij","Tkachuk","03/08/2022",89);
        familyVolodimer.addChild(matvij);
        matvij.setFamily(familyVolodimer);
        HashSet<Pet> wikcsPets = new HashSet<>();
        wikcsPets.add(dog);
        familyWick.setPetsSet(wikcsPets);
        wikcsPets.add(new Fish("Rybka",1,20,new HashSet()));
        HashSet<Pet> arnoldPets = new HashSet<>();
        familyVolodimer.setPetsSet(arnoldPets);
        arnoldPets.add(cat);
        List<Family> familyList = new ArrayList<>();
        familyList.add(familyWick);
        familyList.add(familyVolodimer);

      RoboCat roboCat = new RoboCat("Catrobo",2,100,new HashSet<String>(Set.of("sleep","eat")));
        FamilyController familyController = new FamilyController(new FamilyService(new CollectionFamilyDao(familyList)));

        familyController.createNewFamily(new Woman("Suzi","Staer","03/03/1686",89),new Man("Eric","lavr","03/03/1988",89));

        familyController.bornChild((Family) familyController.getAllFamilies().get(0),"Shon","Emma");

        familyController.adoptChild((Family) familyController.getAllFamilies().get(1), new Man("Peter","Pen","03/03/1686",89));
       familyController.displayAllFamilies();

        System.out.println(familyController.getPets(1));

        familyController.addPet(2,roboCat);
        System.out.println(familyController.getPets(2));
        viktor.describeAge();
        familyController.adoptChild(familyWick,new Man("V","V","02/12/2020",87));
        System.out.println(((Family) familyController.getAllFamilies().get(2)).getChildren().get(3));


















//        int count = 0;
//        while (count < 10000000) {
//            Man man = new Man();
//        }




    }



}
